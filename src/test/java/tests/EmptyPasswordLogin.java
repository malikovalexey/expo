package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pageclasses.LoginPage;

import java.util.concurrent.TimeUnit;

public class EmptyPasswordLogin {
    private WebDriver driver;
    private String loginUrl;
    LoginPage loginPage;

    @BeforeClass
    public void beforeClass() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        loginUrl = "http://dev.vlnt.expoforum.ru/login";

        loginPage = new LoginPage(driver);

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(loginUrl);
    }

    @Test
    public void test() {
        loginPage.setNameInput("imyzzlwv"); //Ввод email в поле логина
        loginPage.clickSubmitBtn(); //Клик на кнопку "Войти"
        loginPage.findLoginErrorMessage(); // Поиск сообщения с ошибкой
        loginPage.getNameInputValue(); // Проверка состояния поля логина
        loginPage.getPasswordInputValue();// Проверка состояния поля пароля

    }
    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
