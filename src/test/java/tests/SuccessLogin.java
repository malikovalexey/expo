package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pageclasses.LoginPage;

import java.util.concurrent.TimeUnit;

public class SuccessLogin {
    private WebDriver driver;
    private String loginUrl;
    LoginPage loginPage;

    @BeforeClass
    public void beforeClass() {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        loginUrl = "http://dev.vlnt.expoforum.ru/login";

        loginPage = new LoginPage(driver);

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(loginUrl);
    }

    @Test
    public void test() {
        loginPage.setNameInput("imyzzlwv@supere.ml"); //Ввод email в поле логина
        loginPage.setPasswordInput("q6qcirin"); //Ввод пароля
        loginPage.clickSubmitBtn(); //Клик на кнопку "Войти"
        driver.findElement(By.xpath("//h4[.='Анкета кандидата']")); //Ищем заголовок "Анкета"


        String expectedUrl = "http://dev.vlnt.expoforum.ru/summary?step=1";
        String actualUrl = driver.getCurrentUrl();
        Assert.assertEquals(actualUrl, expectedUrl, "Actual URL is not the same as expected");
        }

       @AfterClass
    public void tearDown() {
        driver.quit();
    }
    }
