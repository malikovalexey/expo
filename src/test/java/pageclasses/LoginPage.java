package pageclasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class LoginPage {
    public WebDriver driver;
    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(name = "username")
    private WebElement nameInput; // Поле ввода логина

    @FindBy(name = "password")
    private WebElement passwordInput; // Поле ввода пароля

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement submitBtn; // Кнопка "Войти"

    @FindBy (xpath = "//p[.='Введены некорректные данные']")
    private WebElement loginErrorMessage; // Сообщение об ошибке в поле логина

    public void setNameInput(String name) {
        nameInput.sendKeys(name);
    }

    public void  getNameInputValue() {
        String findNameInputValue = nameInput.getAttribute("aria-invalid");
        Assert.assertEquals(findNameInputValue, "true");

    }

    public  void getPasswordInputValue(){
        String findPasswordInputValue = passwordInput.getAttribute("aria-invalid");
        Assert.assertEquals(findPasswordInputValue, "true");
    }

    public void setPasswordInput (String password) {
        passwordInput.sendKeys(password);
}

    public void clickSubmitBtn() {
        submitBtn.click();
}
    public void findLoginErrorMessage() {
        String getLoginErrorMessage = loginErrorMessage.getText();
        Assert.assertEquals("Введены некорректные данные", getLoginErrorMessage);

}
}
